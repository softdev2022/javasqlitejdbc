/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class SelectDatabase {
    
    public static void main(String[] args) {
        Connection con = null;
        String url = "jdbc:sqlite:Dcoffee02.db";
        //Connect Database
        try {
            con = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been establish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }

        //Selection 
        String sql = "SELECT * FROM CATEGORY";
        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                System.out.println(rs.getInt("CATEGORY_ID") 
                        + " " + rs.getString("CATEGORY_NAME"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage()); 
        }

        //Close Database
        if (con != null) {
            try {
                con.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
