/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author ACER
 */
public class UpdateDatabase {
    public static void main(String[] args) {
        Connection con = null;
        String url = "jdbc:sqlite:Dcoffee02.db";
        //Connect Database
        try {
            con = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been establish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }

        //Update
        String sql = "UPDATE CATEGORY SET CATEGORY_NAME=? WHERE CATEGORY_ID=?";
        try {
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, "My Coffee");
            stmt.setInt(2, 1);
            
            int status = stmt.executeUpdate();
//            ResultSet key = stmt.getGeneratedKeys();
//            key.next();
//            System.out.println("" + key.getInt(1));
        } catch (SQLException ex) {
            System.out.println(ex.getMessage()); 
        }

        //Close Database
        if (con != null) {
            try {
                con.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
