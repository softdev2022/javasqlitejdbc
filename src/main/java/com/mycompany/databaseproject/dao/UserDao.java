/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject.dao;

import com.mycompany.databaseproject.helper.DatabaseHelper;
import com.mycompany.databaseproject.model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ACER
 */
public class UserDao implements Dao<User>{

    @Override
    public User get(int id) {
        User user = null;
        String sql = "SELECT * FROM USER WHERE USER_ID = ?";
        Connection con = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                user = user.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage()); 
        }
        return user;    
    }

    @Override
    public List<User> getAll() {
        ArrayList<User> list = new ArrayList();
        String sql = "SELECT * FROM USER";
        Connection con = DatabaseHelper.getConnection();
        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                User user = User.fromRS(rs);
                list.add(user);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage()); 
        }
        return list;
    }

    @Override
    public List<User> getAll(String where, String order) {
        ArrayList<User> list = new ArrayList();
        String sql = "SELECT * FROM USER WHERE "+ where +" ORDER BY " +order;
        System.out.println(sql);
        Connection con = DatabaseHelper.getConnection();
        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                User user = User.fromRS(rs);
                list.add(user);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage()); 
        }
        return list;
    }
    
    @Override
    public User save(User obj) {
        String sql = "INSERT INTO USER (USER_NAME, USER_GENDER, USER_PASSWORD, USER_ROLE)"
                + "VALUES(?,?,?,?)";
        Connection con = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getGender());
            stmt.setString(3, obj.getPassword());
            stmt.setInt(4, obj.getRole());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;    
    }

    @Override
    public User update(User obj) {
        String sql = "UPDATE USER" +
                            " SET USER_NAME = ?, USER_GENDER = ?, USER_PASSWORD = ?, USER_ROLE = ?" +
                            " WHERE USER_ID = ?";
        Connection con = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getGender());
            stmt.setString(3, obj.getPassword());
            stmt.setInt(4, obj.getRole());
            stmt.setInt(5, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(User obj) {
        User user = null;
        String sql = "DELETE FROM USER WHERE USER_ID = ?";
        Connection con = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage()); 
        }
        return -1;    
    }
    public User getByName(String name) {

        User user = null;
        String sql = "SELECT * FROM USER WHERE USER_NAME=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement( sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                user = User.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return user;
    }
}
